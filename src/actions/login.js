import axios from 'axios'

import {loading, error, baseUrl} from './utils.js'

import history from '../routes/history'

export const login = (email, password) => {
  return (dispatch) => {
    dispatch(loading(true))

    axios.post(baseUrl + '/login', {
      email,
      password
    })
      .then(({data}) => {
        dispatch(saveToken(data.token))
        dispatch(loading(false))
        if(data.auth === true) {
          history.push('/tracks')
        }
      })
      .catch((errorinfo) => {
        dispatch(loading(false))
        dispatch(error(errorinfo.response.data))
      })
  }
}

export const logout = () => {
  history.push('/')
  return {
    type: 'LOGOUT'
  }
}

export const saveUserObject = (userObject) => {
  return {
    type: 'SAVE_USER_OBJECT',
    userObject
  }
}

export const saveToken = ( token ) => {
  localStorage.setItem('token', token)

  return {
    type: 'SAVE_TOKEN',
    token
  }
}
