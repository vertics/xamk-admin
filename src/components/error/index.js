import React from 'react'
import PropTypes from 'prop-types'

import ErrorIcon from 'react-icons/lib/md/error-outline'

const ErrorMessage = ({ text, open }) => {
	return (
    <div className={open ? "error open" : "error closed"}>
        {open &&
            <React.Fragment>
                <ErrorIcon className="error-icon" />
                <span>{text}</span>
            </React.Fragment>
        }
    </div>
	)
}

ErrorMessage.propTypes = {
  text: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired
}

export default ErrorMessage
