import React from 'react';
import PropTypes from 'prop-types'

import ExpansionPanel, {
    ExpansionPanelSummary,
    ExpansionPanelDetails
} from 'material-ui/ExpansionPanel'
import Typography from 'material-ui/Typography'
import ExpandMoreIcon from 'react-icons/lib/md/expand-more'
import { TextField } from 'material-ui'
import { AddButton, RemoveButton } from '../Button'
import RadioButton from './RadioButton'

class Options extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      idMap: [{
        id: 0,
        text: ''
      }]
    }
  }

  componentWillMount() {
    let idMap = []
    let data = this.props.data
    for(let i = 0; i < data.length; i++ ) {
      idMap.push({
        id: i,
        text: data[i]
      })
    }

    this.setState({idMap})
  }

  add() {
    let idMap = this.state.idMap
    let data = this.props.data
    idMap.push({
      id: data.length,
      text: ''
    })
    data.push('')
    this.props.cb(data)
  }

  remove() {
    if (this.props.data.length <= 1) return
    const data = this.props.data.slice(0, this.props.data.length - 1)
    const idMap = this.state.idMap.slice(0, this.props.data.length - 1)
    this.setState({idMap})
    this.props.cb(data)
  }

  onChange = id => e => {
    let idMap = this.state.idMap
    let data = this.props.data

    idMap.forEach(item => {
      if(item.id === id) {
        data[item.id] = e.target.value
        item.text = e.target.value
      }
    })

    this.props.cb(data)
  }

  changeCorrectAnswer = e => {
    this.props.caCb(e)
  }

  render () {
    const { caData } = this.props
    const { idMap } = this.state
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>Answering Options</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div>
            <div className={'options-titleContainer'}>
              <p className={'options-title1'}>Option</p>
              <p className={'options-title2'}>Select Correct Answer</p>
            </div>
            {
              idMap.map(item => (
                <div className={'options-inputContainer'} key={item.id}>
                  <TextField
                    value={item.text}
                    label={`Answering Option #${item.id + 1}`}
                    margin={'normal'}
                    onChange={this.onChange(item.id)}
                  />
                  <RadioButton data={caData} index={item.id} cb={this.changeCorrectAnswer} />
                </div>
              ))
            }
            <AddButton onClick={() => this.add()} />
            <RemoveButton onClick={() => this.remove()} display={idMap.length > 1} />
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    )
  }
}

Options.propTypes = {
    data: PropTypes.array.isRequired,
    cb: PropTypes.func.isRequired
}

export default Options