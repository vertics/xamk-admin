import React from 'react'
import PropTypes from 'prop-types'
import RadioButtonChecked from 'react-icons/lib/md/radio-button-checked'
import RadioButtonUnchecked from 'react-icons/lib/md/radio-button-unchecked'

const RadioButton = ({ data, index, cb }) => {
  if (data === index) {
    return (
      <div className={'options-iconContainer'}>
        <RadioButtonChecked color={'#194A72'} size={24} />
      </div>
    )
  }
  return (
    <div onClick={() => cb(index)} className={'options-iconContainer'}>
      <RadioButtonUnchecked color={'rgba(0, 0, 0, 0.54)'} size={24} />
    </div>
  )
}

RadioButton.propTypes = {
  data: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  cb: PropTypes.func.isRequired
}

export default RadioButton
