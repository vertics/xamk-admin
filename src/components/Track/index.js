import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Axios from 'axios'
import Waypoint from './waypoint'
import Description from './description'
import _ from 'lodash'
import { withStyles } from 'material-ui/styles'
import { AddButton, SaveButton } from '../Button'
import history from '../../routes/history'
import { baseUrl } from '../../actions/utils'

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  }
})

class Track extends Component {
  constructor () {
    super()
    this.state = {
      track: {
        description: {
          name: '',
          qrCode: 0
        },
        waypoints: []
      },
      loading: true,
      error: false
    }

    this.updateDescription = this.updateDescription.bind(this)
    this.updateWaypoint = this.updateWaypoint.bind(this)
    this.addWaypoint = this.addWaypoint.bind(this)
    this.deleteWaypoint = this.deleteWaypoint.bind(this)
    this.saveTrack = this.saveTrack.bind(this)
  }

  componentDidMount () {
    Axios.get(baseUrl + `/track/${this.props.id}`)
      .then(res => {
        return res.data
      })
      .then(res => {
        this.setState({
          track: res,
          loading: false
        })
      })
      .catch(err => {
        this.props.error('Server error')
        this.setState({
          loading: false,
          error: err.message
        })
      })
  }

  updateDescription (data) {
    this.setState(prev => _.clone(prev).track.description = data)
  }

  updateWaypoint (data, i) {
    this.setState(prev => _.clone(prev).track.waypoints[i] = data)
  }

  addWaypoint () {
    this.setState(prev => _.clone(prev).track.waypoints.push({}))
  }

  deleteWaypoint (i) {
    this.setState(prev => _.clone(prev).track.waypoints.splice(i, 1))
  }

  saveTrack () {
    const url = baseUrl + `/track/${this.state.track._id}`;
    const config = { headers: {
      'credentials': 'include',
      'x-access-token': localStorage.getItem('token')
    }}
    const data = this.state.track
    Axios.put(url, data, config)
      .then(res => {
        this.props.error('Track saved')
        history.push('/tracks')
      })
      .catch(err => {
        this.props.error('Error saving track')
        this.setState({
          loading: false,
          error: err.message
        })
      })

  }

  render () {
    const { track } = this.state

    return (
      <div className={'container'}>
        <Description cb={this.updateDescription} data={track.description} />
        {
          track.waypoints.map((el, i) =>
            <Waypoint key={i} index={i} data={el} delete={() => this.deleteWaypoint(i)} cb={(data) => this.updateWaypoint(data, i)} />
          )
        }
        <div style={{ marginTop: 5, display: 'flex', alignItems: 'flex-start', width: '100%' }}>
          <AddButton onClick={this.addWaypoint} label={'NEW WAYPOINT'} />
          <SaveButton onClick={this.saveTrack} label={'SAVE TRACK'} />
        </div>
      </div>
    )
  }
}

Track.propTypes = {
  id: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Track)
