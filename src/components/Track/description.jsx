import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from 'material-ui/ExpansionPanel'
import Typography from 'material-ui/Typography'
import ExpandMoreIcon from 'react-icons/lib/md/expand-more'
import { TextField } from 'material-ui'
import QRCodeComponent from '../QR'

class Description extends Component {
  handleChange (e, name) {
    this.props.data[name] = e.target.value
    this.props.cb(this.props.data)
  }

  handeQrChange (e, name) {
    this.props.data[name] = e
    this.props.cb(this.props.data)
  }

  render () {
    const { data, creatingNewTrack } = this.props

    return (
      <ExpansionPanel className={'fullWidth'}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>Description</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div style={styles.container}>
            <TextField
              required
              value={data.name}
              label={'Track Name'}
              margin={'normal'}
              fullWidth
              onChange={e => this.handleChange(e, 'name')}
            />
            <QRCodeComponent
              value={data.qrCode}
              cb={e => this.handeQrChange(e, 'qrCode')}
              creatingNewTrack={creatingNewTrack}
            />
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    )
  }
}

const styles = {
  container: {
    width: '100%',
    maxWidth: 500
  }
}

Description.propTypes = {
  cb: PropTypes.func.isRequired,
  data: PropTypes.shape({
    name: PropTypes.string.isRequired,
    qrCode: PropTypes.number.isRequired
  })
}

export default Description
