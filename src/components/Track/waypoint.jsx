import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  ExpansionPanelActions
} from 'material-ui/ExpansionPanel'
import Typography from 'material-ui/Typography'
import ExpandMoreIcon from 'react-icons/lib/md/expand-more'
import { TextField, Button, Divider } from 'material-ui'
import _ from 'lodash'

import Map from '../Map'
import Options from './Options'
import ImageUpload from '../ImageUpload'

class Waypoint extends Component {
  constructor (props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
    this.handleLocationChange = this.handleLocationChange.bind(this)
  }

  handleChange = name => e => {
    this.props.data[name] = e.target.value
    this.props.cb(this.props.data)
  }

  handleLocationChange = name => val => {
    this.props.data[name] = val
    this.props.cb(this.props.data)
  }

  render() {
    const { data, index } = this.props
    return (
      <ExpansionPanel className={'fullWidth'}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>{`Waypoint #${index + 1}`}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div style={styles.container}>
            <p className="MuiTypography-body1-689">Basic Information</p>
            <TextField
              required
              value={data.name}
              label={'Name'}
              margin={'normal'}
              fullWidth
              onChange={this.handleChange('name')}
            />
            <TextField
              required
              value={data.description}
              label={'Description'}
              multiline
              margin={'normal'}
              fullWidth
              onChange={this.handleChange('description')}
            />
            <ImageUpload cb={this.handleLocationChange('image')} value={data.image} label="Image*" />
            <p className="MuiTypography-body1-689">Waypoint location</p>
            <Map
              location={data.location}
              onChange={this.handleLocationChange('location')}
            />
            <p className="MuiTypography-body1-689">Question and Answering Options</p>
            <TextField
              required
              value={data.question}
              label={'Question'}
              margin={'normal'}
              fullWidth
              onChange={this.handleChange('question')}
            />
            <Options cb={this.handleLocationChange('options')} data={data.options} caData={data.correctAnswer} caCb={this.handleLocationChange('correctAnswer')} />
          </div>
        </ExpansionPanelDetails>
        <Divider />
        <ExpansionPanelActions>
          <Button onClick={() => this.props.delete()} size="small">Delete waypoint</Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    )
  }

}

const styles = {
  container: {
    width: '100%',
    maxWidth: 500
  }
}

Waypoint.propTypes = {
  index: PropTypes.number.isRequired,
  cb: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  delete: PropTypes.func.isRequired
}

export default Waypoint
