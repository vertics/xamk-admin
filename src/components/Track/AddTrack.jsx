import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Axios from 'axios'
import Waypoint from './waypoint'
import Description from './description'
import _ from 'lodash'
import { withStyles } from 'material-ui/styles'
import { AddButton, SaveButton } from '../Button'
import history from '../../routes/history'
import { baseUrl } from '../../actions/utils'

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  }
})

const emptyWaypoint = {
  name: '',
  description: '',
  image: '',
  location: {
    latitude: 60.169856,
    longitude: 24.938379,
    radius: 200
  },
  question: '',
  options: [''],
  correctAnswer: 0
}

class AddTrack extends Component {
  constructor (props) {
    super(props)
    this.state = {
      track: {
        description: {
          name: '',
          qrCode: 0
        },
        waypoints: []
      },
      loading: true,
      error: false
    }

    this.updateDescription = this.updateDescription.bind(this)
    this.updateWaypoint = this.updateWaypoint.bind(this)
    this.addWaypoint = this.addWaypoint.bind(this)
    this.deleteWaypoint = this.deleteWaypoint.bind(this)
    this.saveTrack = this.saveTrack.bind(this)
  }

  updateDescription (data) {
    let changedData = Object.assign({}, this.state)
    changedData.track['description'] = data
    this.setState(changedData)
  }

  updateWaypoint (data, i) {
    this.setState(prev => _.clone(prev).track.waypoints[i] = data)
  }

  addWaypoint () {
    let data = Object.assign({}, this.state)
    const empty = Object.assign({}, emptyWaypoint)
    data.track.waypoints.push(empty)
    this.setState(data, () => {
      console.log(this.state.track.waypoints)
    })
  }

  deleteWaypoint (i) {
    this.setState(prev => _.clone(prev).track.waypoints.splice(i, 1))
  }

  saveTrack () {
    const url = baseUrl + `/tracks`
    const config = { headers: {
      'credentials': 'include',
      'x-access-token': localStorage.getItem('token')
    }}
    const data = this.state.track
    Axios.post(url, data, config)
      .then(res => {
        this.props.error('Track created')
        history.push('/tracks')
      })
      .catch(err => {
        this.props.error('Error saving')
        this.setState({
          loading: false,
          error: err.message
        })
      })

  }

  render () {
    return (
      <div className={'container'}>
        <Description cb={this.updateDescription} data={this.state.track.description} creatingNewTrack />
        { this.state.track.waypoints.map((el, i) =>
          <Waypoint key={i} index={i} data={el} delete={() => this.deleteWaypoint(i)} cb={(data) => this.updateWaypoint(data, i)} />)
        }
        <div style={{ marginTop: 5, display: 'flex', alignItems: 'flex-start', width: '100%' }}>
          <AddButton onClick={this.addWaypoint} label={'ADD NEW WAYPOINT'} />
          <SaveButton onClick={this.saveTrack} label={'SAVE NEW TRACK'} />
        </div>
      </div>
    )
  }
}

AddTrack.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(AddTrack)
