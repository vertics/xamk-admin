import React from 'react'
import PropTypes from 'prop-types'
import QRCode from 'qrcode-react'
import { Button } from 'material-ui'
import { baseUrl } from '../../actions/utils'

class QRCodeComponent extends React.Component {
  constructor(props) {
    super()

    this.downloadCode = this.downloadCode.bind(this)
    this.newCode = this.newCode.bind(this)
    this.makeId = this.makeId.bind(this)
  }

  componentDidMount() {
    if (this.props.creatingNewTrack) {
      this.newCode()
    }
  }

  downloadCode(index) {
    const canvas = document.getElementById('qrCanvas').getElementsByTagName('canvas')[0];
    const dataURL = canvas.toDataURL("image/png");

    const downloadLink = 'data:application/octet-stream' + dataURL.slice(dataURL.indexOf(';'), dataURL.length);

    var a = document.createElement('A');
    a.href = downloadLink
    a.download = 'qrcode.png'
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  }

  makeId() {
    const number = '123456789';
    let newValue = "";
    for (let i = 0; i < 12; i++) {
      newValue = newValue + number.charAt(Math.floor(Math.random() * number.length));
    }
    return Number(newValue)
  }

  async newCode() {
    let code = null
    let ready = false
    // loop until unique code
    while (!ready) {
      code = await this.makeId()
      await fetch(baseUrl + '/tracks')
      .then(result => {
        return result.json()
      })
      .then(result => {
        for (var key in result) {
          if (code != result[key].description.qrCode) {
            ready = true
          }
        }
      })
      .catch(err => {
        ready = true
        alert('Unexpected error! Try again later')
      })
    }
    this.props.cb(code)
  }

  render() {
    const { value } = this.props
    return (
      <div style={{ display: 'flex', marginTop: 20, flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
        <p style={{ color: 'rgba(0, 0, 0, 0.54)', fontSize: 12, width: '100%' }}>QR-Code*</p>
        <div id="qrCanvas">
          <QRCode value={value.toString()} />
          <p style={{ textAlign: 'center', color: 'rgba(0, 0, 0, 0.54)' }}>{`Code: ${value}`}</p>
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: 128, height: 128 }}>
          <Button color="primary" style={{ marginBottom: 5 }} onClick={() => this.newCode()}>
            Reload
          </Button>
          <Button style={{ marginTop: 5 }} onClick={() => this.downloadCode()}>
            Download
          </Button>
        </div>
      </div>
    )
  }
}

QRCodeComponent.propTypes = {
  cb: PropTypes.func.isRequired,
  value: PropTypes.number.isRequired,
  creatingNewTrack: PropTypes.bool
}

export default QRCodeComponent

