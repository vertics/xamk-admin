import React from 'react'
import Dropzone from 'react-dropzone'
import TextField from 'material-ui/TextField'
import FileUploadIcon from 'react-icons/lib/md/file-download'
import Axios from 'axios'
import PropTypes from 'prop-types'

import { baseUrl } from '../../actions/utils'

class ImageUpload extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      fileName: 'Select new photo'
    }

    this.onDrop = this.onDrop.bind(this)
  }

  async onDrop(acceptedFiles, rejectedFiles) {
    if (rejectedFiles[0]) {
      return alert('Image size too big! Maximum size 5mb')
    }

    acceptedFiles = acceptedFiles[0]
    this.setState({ fileName: acceptedFiles.name })


    const data = new FormData()
    data.append('image', acceptedFiles)

    const url = baseUrl + '/uploadimage'
    const config = { headers: {
      'Content-Type': 'multipart/form-data',
      'credentials': 'include',

      'x-access-token': localStorage.getItem('token')
    }}
    Axios.post(url, data, config)
      .then((res) => {
        return res.data.imageid
      })
      .then((imageid) => {
        setTimeout(() => {
          this.props.cb(imageid)
        }, 2000)
      })
      .catch(err => alert('Error uploading image, try again later'))
  }

  render() {
    const { value, label } = this.props
    const { fileName } = this.state
    const imageLabel = label ? label : 'Image'

    return (
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', flexWrap: 'wrap', alignItems: 'flex-start' }}>
        <div style={{ display: 'block', marginTop: 5, marginBottom: 5, marginRight: 20 }}>
          <p style={{ color: 'rgba(0, 0, 0, 0.54)', fontSize: 12 }}>{imageLabel}</p>
          <div style={{ width: '100%', height: '100%', maxHeight: 300, maxWidth: 300 }}>
            <img alt="Upload" style={{ maxWidth: 'inherit', maxHeight: 'inherit', display: !value || value === '' ? 'none' : 'inline-block' }} src={!value || value === '' ? '#' : baseUrl + '/images/' + value} />
          </div>
        </div>
        <Dropzone
          accept="image/*"
          maxSize={5000000}
          multiple={false}
          style={{ visibility: 'hidden' }}
          onDrop={(acceptedFiles, rejectedFiles) => this.onDrop(acceptedFiles, rejectedFiles)}
        >
          <div style={{ visibility: 'visible', display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 5, marginBottom: 5 }}>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
              <div className="UploadBtn" style={{ height: 50, width: 50, backgroundColor: '#194A72', borderRadius: '50%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <FileUploadIcon color="white"/>
              </div>
              <TextField
                required
                disabled
                id="text-field-disabled"
                value={fileName}
                style={{ marginLeft: 10, marginBottom: 4 }}
              />
            </div>
          </div>
        </Dropzone>
      </div>
    )
  }
}

ImageUpload.propTypes = {
  cb: PropTypes.func.isRequired,
}

export default ImageUpload
