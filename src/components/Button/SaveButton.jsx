import React from 'react'
import PropTypes from 'prop-types'

import SaveIcon from 'react-icons/lib/md/save';

import Button from 'material-ui/Button'

const SaveButton = ({ onClick, label = 'SAVE' }) => (
    <Button onClick={() => onClick()}>
        {label}
        <SaveIcon />
    </Button>
)

SaveButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  label: PropTypes.string
}

export default SaveButton
