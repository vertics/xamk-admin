import React from 'react'
import PropTypes from 'prop-types'

import MdRemove from 'react-icons/lib/md/remove';

import Button from 'material-ui/Button'

const RemoveButton = ({ onClick, label = 'REMOVE', display = true }) => (
    <Button onClick={() => onClick()} style={{ display: display ? '' : 'none' }}>
        {label}
        <MdRemove />
    </Button>
)

RemoveButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  label: PropTypes.string
}

export default RemoveButton
