import React from 'react'
import PropTypes from 'prop-types'

import PlusIcon from 'react-icons/lib/md/add';

import Button from 'material-ui/Button'

const AddButton = ({ onClick, label = 'ADD' }) => (
    <Button onClick={() => onClick()}>
        {label}
        <PlusIcon />
    </Button>
)

AddButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  label: PropTypes.string
}

export default AddButton
