import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { withStyles } from 'material-ui/styles'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'

const styles = {
  button: {
    background: "#194A72",
    padding: "1em",
    color: "white"
  }
}

class Login extends Component {

  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: ''
    }
  }

  handleChange = name => e => {
    this.setState({
      [name]: e.target.value
    })
  }

  listenEnterKey(e) {
    if (e.key != 'Enter') return
    this.login()
  }

  login() {
    const { email, password } = this.state

    this.props.login(email, password)
  }

  render () {
    return (
      <div className="login-container">
        <form className="login-form" noValidate >
          <div className="login-inputContainer">
          <div className="login-imagecontainer">
            <p className="login-logoText">eduQuiz</p>
          </div>
            <TextField
              onKeyPress={(e) => this.listenEnterKey(e)}
              id="name"
              label="Email"
              className="login-email"
              value={this.state.email}
              onChange={this.handleChange('email')}
              type="email"
              margin="normal"
            />

            <TextField
              onKeyPress={(e) => this.listenEnterKey(e)}
              id="password"
              label="Password"
              className="login-password"
              value={this.state.password}
              onChange={this.handleChange('password')}
              type="password"
              margin="normal"
            />

            <Button variant="raised" color="primary" className={this.props.classes.button} onClick={() => this.login()}>
              LOG IN
            </Button>
          </div>
        </form>
      </div>
    )
  }
}

Login.propTypes = {
  state: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired
}

export default withStyles(styles)(Login)
