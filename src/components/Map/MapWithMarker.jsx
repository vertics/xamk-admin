import React from 'react'
import PropTypes from 'prop-types'

import { withGoogleMap, GoogleMap, Marker, Circle } from "react-google-maps"

const MapWithMarker = withGoogleMap( ({marker, setMarker, radius}) => (
    <GoogleMap
      defaultZoom={13}
      defaultCenter={{ lat: Number(marker.latitude), lng: Number(marker.longitude) }}
      onClick={(e) => setMarker(e)}
    >
        <Marker
            draggable={true}
            position={{ lat: Number(marker.latitude), lng: Number(marker.longitude) }}
            onDragEnd={(e) => setMarker(e)}
        />

        <Circle
            radius={Number(radius)}
            center={{ lat: Number(marker.latitude), lng: Number(marker.longitude) }}
            options={{ fillColor: '#4286F4', strokeColor: '#2D5EAD', strokeOpacity: 0.5 }}
        />
    </GoogleMap>
))

MapWithMarker.propTypes = {
    marker: PropTypes.object.isRequired,
    setMarker: PropTypes.func.isRequired,
    radius: PropTypes.number.isRequired
}

export default MapWithMarker