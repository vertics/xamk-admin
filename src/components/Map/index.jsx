import React, { Component } from 'react'
import PropTypes from 'prop-types'

import TextField from 'material-ui/TextField'
import { withStyles } from 'material-ui/styles'

import MapWithMarker from './MapWithMarker'

const styles = {
  textfield: {
    width: '15%'
  },
  slider: {
    width: '80%',
    height: 40
  }
}

class Map extends Component {

  setMarker(e) {
    const latitude = Math.round(e.latLng.lat() * 1000000) / 1000000 // round to 6 deciamals
    const longitude = Math.round(e.latLng.lng() * 1000000) / 1000000

    this.onChange({ latitude, longitude })
  }

  changeRadius(event) {
    const radius = Number(event.target.value)
    this.onChange({ radius })
  }

  onChange = newValues => {
    const newLocation = Object.assign({}, this.props.location, newValues)

    this.props.onChange(newLocation)
  }

  render() {

    const { label, location: { latitude, longitude, radius}, classes } = this.props

    return (
      <span className="map-container">
        <p className="map-baseText">{label ? label : ''}</p>
        <MapWithMarker
          containerElement={<div className="map-mapview-container" />}
          mapElement={<div className="map-mapview" />}
          setMarker={e => this.setMarker(e)}
          marker={{latitude, longitude}}
          radius={radius}
        />

        <div className="map-textfields">
          <TextField className={classes.textField}
            required
            type="number"
            value={radius}
            label={'Radius (meters)'}
            onChange={(e) => this.changeRadius(e)}
          />
        </div>
        <p className="map-baseText">Move the marker on map and set radius</p>
      </span>
    )
  }
}

Map.propTypes = {
  label: PropTypes.string,
  location: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Map)
