import React, { Component } from 'react'
import { CircularProgress } from 'material-ui/Progress'

import List, { ListItem, ListItemSecondaryAction, ListItemText } from 'material-ui/List'
import IconButton from 'material-ui/IconButton'
import Icon from 'material-ui/Icon'
import Paper from 'material-ui/Paper'
import { Link } from 'react-router-dom'
import Button from 'material-ui/Button'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from 'material-ui/Dialog'

import { baseUrl } from '../../actions/utils'
import history from '../../routes/history'
import { AddButton } from '../Button'

import Axios from 'axios'

class Tracks extends Component {
  constructor () {
    super()
    this.state = {
      tracks: [],
      loading: true,
      error: false,
      dialog: false,
      delete: null
    }

    this.add = this.add.bind(this)
  }

  componentDidMount () {
    this.setState({ loading: true })
    Axios.get(baseUrl + '/tracks')
      .then(res => res.data)
      .then(tracks => {
        this.setState({
          loading: false,
          tracks
        })
      })
      .catch(error => {
        this.props.error('Error loading tracks')
        this.setState({
          error: error.message,
          loading: false
        })
      })
  }

  delete () {
    this.setState({
      dialog: false,
      loading: true
    })
    Axios.delete(baseUrl + `/track/${this.state.delete}`, {
      headers: {
        'x-access-token': localStorage.getItem('token')
      }
    })
      .then(res => {
        this.setState(prev => {
          return {
            tracks: prev.tracks.filter(el => el._id !== prev.delete),
            loading: false
          }
        })
      })
      .catch(error => {
        this.props.error('Server error')
        this.setState({
          error: error.message,
          loading: false
        })
      })
  }

  add() {
    history.push('/track/new')
  }

  tracks () {
    return (
      <Paper className={'fullWidth'}>
        <Dialog
          open={this.state.dialog}
          onClose={this.handleClose}
          aria-labelledby={'alert-dialog-title'}
          aria-describedby={'alert-dialog-description'}
        >
          <DialogTitle id={'alert-dialog-title'}>Are you sure that you want to delete this track.</DialogTitle>
          <DialogContent>
            <DialogContentText id={'alert-dialog-description'}>
              Please note that it's not possible to recover tracks later
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.setState({ dialog: false })} color={'primary'}>
              Cancel
            </Button>
            <Button onClick={() => this.delete()} color={'primary'} autoFocus>
              Delete
            </Button>
          </DialogActions>
        </Dialog>

        <List>
          { this.state.tracks.map((track) => {
            return (
              <ListItem key={track._id}>
                <ListItemText primary={track.description.name} />
                <ListItemSecondaryAction>
                  <IconButton aria-label={'Edit'}>
                    <Link className={'withoutDecoration'} to={`/track/${track._id}`}>
                      <Icon style={{ marginTop: 3 }}>mode_edit</Icon>
                    </Link>
                  </IconButton>
                  <IconButton aria-label={'Delete'}>
                    <Icon onClick={() => this.setState({ dialog: true, delete: track._id })}><i className={'material-icons'}>delete_forever</i></Icon>
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            )
          })}
          <ListItem>
            <AddButton onClick={this.add} label={'ADD NEW TRACK'} />
          </ListItem>
        </List>
      </Paper>
    )
  }

  render () {
    return (
      <div className={'container'}>
        { this.state.loading ? <CircularProgress thickness={7} /> : this.tracks() }
      </div>
    )
  }
}

export default Tracks
