import React, {Component} from 'react'
import PropTypes from 'prop-types'

import { withStyles } from 'material-ui/styles'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Button from 'material-ui/Button'

import { Link } from 'react-router-dom'

const styles = {
  header: {
    background: "#194A72"
  },
  button: {
    color: "white"
  }
}

class Header extends Component {
  componentDidMount() {
    if ( !this.props.loggedIn ) {
      this.props.logout()
    }
  }

  render () {
    const path = this.props.location.pathname
    if (path === '/' || path === '/login') return null
    return (
      <div className={'header'} >
        <AppBar position={'static'} className={this.props.classes.header}>
          <Toolbar className={'header-toolbar'}>
            <Link className={'withoutDecoration'} to={'/tracks'}> <p className={'header-logoText'}>eduQuiz</p></Link>
            <Button className={this.props.classes.button} onClick={ () => {
              localStorage.clear('token')
              this.props.logout()
            } } >Logout</Button>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default withStyles(styles)(Header)

Header.propTypes = {
  location: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired
}
