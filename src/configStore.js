import { createStore, applyMiddleware } from 'redux'

import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import thunk from 'redux-thunk'

import main from './reducers/main'

const persistConfig = {
  key: 'nyxgen_root',
  storage,
  blacklist: ['signin', 'error', 'loading', 'booking']
}

const persistedReducer = persistReducer(persistConfig, main)

let store = createStore(persistedReducer, applyMiddleware(thunk))

export { store }
