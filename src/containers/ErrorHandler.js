import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Error from '../components/error'

const mapStateToProps = (state) => {
  return {
    text: state.error.text,
    open: state.error.open
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

const ErrorHandler = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Error))

export default ErrorHandler
