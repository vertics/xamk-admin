import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { logout } from '../actions/login'

import Header from '../components/Header'

const mapStateToProps = (state, props) => {
  return {
    location: props.location,
    loggedIn: state.user.loggedIn
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => (
      dispatch(logout())
    )
  }
}

const HeaderHandler = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Header))

export default HeaderHandler
