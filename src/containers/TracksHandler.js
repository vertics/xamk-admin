import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { error } from '../actions/utils'

import Tracks from '../components/Tracks'

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    error: (text) => (
      dispatch(error(text))
    )
  }
}

const TracksHandler = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Tracks))

export default TracksHandler
