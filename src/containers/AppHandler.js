import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import App from '../routes/App'

const mapStateToProps = (state) => {
  return {
    loggedIn: state.user.loggedIn
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

const AppHandler = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App))

export default AppHandler
