import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { error } from '../actions/utils'

import AddTrack from '../components/Track/AddTrack'

const mapStateToProps = (state) => {
  return { }
}

const mapDispatchToProps = (dispatch) => {
  return {
    error: (text) => (
      dispatch(error(text))
    )
  }
}

const AddTrackHandler = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(AddTrack))

export default AddTrackHandler
