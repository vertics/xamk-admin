import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { error } from '../actions/utils'

import Track from '../components/Track'

const mapStateToProps = (state, props) => {
  return {
    id: props.match.params.id
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    error: (text, success = false) => (
      dispatch(error(text, success))
    )
  }
}

const TrackHandler = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Track))

export default TrackHandler
