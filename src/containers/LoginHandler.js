import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { login } from '../actions/login'

import Login from '../components/Login'

const mapStateToProps = (state) => {
  return {
    state
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: ( email, password ) => (
      dispatch(login( email, password ))
    )
  }
}

const LoginHandler = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Login))

export default LoginHandler
