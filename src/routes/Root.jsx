import React from 'react'
import PropTypes from 'prop-types'

import {
  Router
} from 'react-router-dom'

import { Provider } from 'react-redux'

import history from './history'

import App from '../containers/AppHandler'

const Root = ({ store }) => (
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>
)

Root.propTypes = {
  store: PropTypes.object.isRequired,
}

export default Root
