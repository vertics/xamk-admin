import React from 'react'
import PropTypes from 'prop-types'

import {
  Route,
  Switch
} from 'react-router-dom'

import Error from '../containers/ErrorHandler'
import Header from '../containers/HeaderHandler'
import Login from '../containers/LoginHandler'
import Track from '../containers/TrackHandler'
import Tracks from '../containers/TracksHandler'
import AddNew from '../containers/AddNewHandler'

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#2A628F',
      main: '#194A72',
      dark: '#16324F',
      contrastText: '#FFF',
    }
  }
})

const isLogged = ({ loggedIn }) => (
    loggedIn ? <Tracks /> : <Login />
)

const App = ( props ) => (
    <div className="root-container">
      <MuiThemeProvider theme={theme}>
        <Header />
        <Error />
        <Switch>
          <Route exact path='/' component={() => isLogged( props )} />
          <Route exact path={'/tracks'} component={() => <Tracks />} />
          <Route exact path={'/track/new'} component={() => <AddNew />} />
          <Route exact path={'/track/:id'} component={() => <Track />} />
          <Route exact path={'/login'} render={() => <Login />} />
        </Switch>
      </MuiThemeProvider>
    </div>
)

App.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
}

export default App
